#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Calculate and show the properties of the sediments.
"""

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import rand


def select(particles, z):
    """
    Find the ids of the particles close to the z plane
    """

    maxR = np.max(particles[:, 3])
    # in mono-dispersed case, one maxR gives wrong crevice
    highid = particles[:, 2] < z + 2 * maxR
    lowid = particles[:, 2] > z - 2 * maxR
    return particles[highid & lowid, :]


def sample(particles, z, R, N, showplot=False):
    """
    Generate N test points on the cut plane z.
    """

    # N random points within a circle
    meanxy = np.mean(particles[:, :2], axis=0)
    rtest = np.sqrt(rand(N, 1)) * R
    theta = 2 * np.pi * rand(N, 1)
    points = meanxy + rtest * np.concatenate(
        (np.cos(theta), np.sin(theta)), axis=1)
    points = np.concatenate((points, z * np.ones((N, 1))), axis=1)

    if showplot:
        xsection(particles, z, R)
        plt.plot(points[:, 0], points[:, 1], 'rx', markersize=1)
        plt.show()

    return points


def xsection(particles, z, k):
    """
    Make a plot of a cross-section of the sediments.
    """
    from matplotlib.collections import PatchCollection
    zpos = particles[:, 2]
    r = particles[:, 3]
    hpos = particles[:, :2]
    teal = [0, 0.5, 0.5]
    zid = np.where(np.abs(zpos - z) <= r)[0]
    dz = zpos[zid] - z
    zr = np.sqrt(r[zid]**2 - dz**2)
    zxy = hpos[zid, :]
    _, ax = plt.subplots()
    patches = [plt.Circle(center, radius) for center, radius in zip(zxy, zr)]
    circles = PatchCollection(patches, color=teal, lw=1.5, fc='none')
    ax.set_aspect(1)
    ax.add_collection(circles)
    meanR = np.median(particles[:, 3])
    testR = k * meanR
    meanxy = np.mean(particles[:, :2], axis=0)
    c = plt.Circle(meanxy, testR, color='r', fill=False, lw=1.5, ls='--')
    ax.add_artist(c)
    plt.title(r'$z={0:.2f}$'.format(z))
    plt.autoscale(tight=True)
