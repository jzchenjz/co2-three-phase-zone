#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Grow a spherical ice or hydrate inside the sediment particles.
"""
from itertools import combinations

import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import norm, matrix_rank
from numpy.random import rand
from scipy.optimize import fsolve

# ----- Particle operations ----- #


def distance(s, ps):
    """
    Calculate the distances from the sphere s to particles ps. The sphere can be only a point.
    """
    if len(s) == 3:  # s is only a point, not a sphere
        s = np.append(s, 0)

    ps = np.reshape(ps, (-1, 4))
    dmin = min(ps[:, 3]) / 1e5
    d = norm(ps[:, :3] - s[:3], axis=-1) - ps[:, 3] - s[3]
    # d with values less than 0 but greater than -dmin are set to 0
    d[(d > -dmin) & (d < 0)] = 0
    return d


def stack(s, ps):
    """
    Add the sphere s to particles ps. The sphere can be only a point
    """
    if len(s) == 3:  # s is only a point, not a sphere
        s = np.append(s, 0)
    return np.vstack((ps, s))


def cut(s, ps):
    """
    Determine if a sphere s is cutting any particles ps.
    """
    d = distance(s, ps)
    return np.any(d < 0)


def near(s, particles, k=20):
    """
    Find the nearest k particles from the test sphere s.
    """
    k = min(k, len(particles))
    # ids and distances of the closest particles
    d = distance(s, particles)
    # sort distance and particles by d
    ids = np.argsort(d)
    ids = ids[:k]
    return ids, d[ids]


def around(s, pnear):
    """
    Find the particles around the current one that touches the current particle.
    """
    dists = distance(s, pnear)
    dmin = min(pnear[:, 3]) / 100
    ids = np.where(np.abs(dists) < dmin)[0]
    return pnear[ids, :]


# ----- Geometry operations ----- #


def height(pos, pos1, pos2):
    """
    Calculate the distance from the point pos to a line passing through pos1 and pos2.
    """

    u = pos - pos1
    v = pos - pos2
    w = pos1 - pos2
    return norm(np.cross(u, v)) / norm(w)


def project(pos1, pos2, pos3):
    """
    Calculate the transform matrix R that converts the center points pos1, pos2, and pos3 into a horizontal plane. This
    is an orthonorma matrix such that R @ R' = I. For a 3D point in a row vector v, its transformed position is
    vt = (R @ v')' = v @ R', and to transform back, v = vt @ R.
    """

    # normal vector of the center plane
    n_vec = np.cross(pos1 - pos3, pos2 - pos3)
    n_vec = n_vec / norm(n_vec)
    z_hat = [0, 0., 1.]
    v = np.cross(n_vec, z_hat)
    s = norm(v)
    c = np.dot(n_vec, z_hat)

    if s < 0.01:
        # almost horizontal
        return np.eye(3)
    # https://math.stackexchange.com/questions/180418
    Rv = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    return np.eye(3) + Rv + Rv @ Rv / (1 + c)


def apollonius(pxy1, r1, pxy2, r2, pxy3, r3, showplot=False):
    """
    Find the circle tangent to three circles with centers at pxy1, pxy2 and pxy3 with radii r1, r2, and r3. Note that
    there are two circles, and the first one (posa, ra) is with the larger radius, the second one (posb, rb) is smaller.
    In case rb is negative, it is set to zero instead.

    Modified from Abhilash Harpale 2012
    https://www.mathworks.com/matlabcentral/fileexchange/37403
    """
    x1, y1 = pxy1
    x2, y2 = pxy2
    x3, y3 = pxy3
    a2 = x1 - x2
    a3 = x1 - x3
    b2 = y1 - y2
    b3 = y1 - y3
    c2 = r1 - r2
    c3 = r1 - r3
    d2 = (x1**2 + y1**2 - r1**2 - x2**2 - y2**2 + r2**2) / 2
    d3 = (x1**2 + y1**2 - r1**2 - x3**2 - y3**2 + r3**2) / 2

    α1 = (b2 * d3 - b3 * d2) / (a3 * b2 - a2 * b3)
    α2 = (a2 * d3 - d2 * a3) / (a2 * b3 - a3 * b2)
    β1 = (b3 * c2 - b2 * c3) / (a3 * b2 - a2 * b3)
    β2 = (a3 * c2 - a2 * c3) / (a2 * b3 - a3 * b2)
    a = β1**2 + β2**2 - 1
    b = 2 * (β1 * (α1 - x1) + β2 * (α2 - y1) - r1)
    c = (α1 - x1)**2 + (α2 - y1)**2 - r1**2

    if b**2 - 4 * a * c < 0:  # no solution
        ra = 0
        rb = 0
    else:
        δ = np.sqrt(b**2 - 4 * a * c)
        rx = (-b + δ * np.array([1, -1])) / 2 / a
        if (rx[0] + r1 >= 0) & (rx[0] + r2 >= 0) & (rx[0] + r3 >= 0):
            ra = rx[0]
            rb = max(rx[1], 0)
        elif (rx[1] + r1 >= 0) & (rx[1] + r2 >= 0) & (rx[1] + r3 >= 0):
            ra = rx[1]
            rb = 0
        else:  # no solution
            ra = 0
            rb = 0
    posa = np.array([α1 + β1 * ra, α2 + β2 * ra])
    posb = np.array([α1 + β1 * rb, α2 + β2 * rb])

    if showplot:
        from matplotlib.collections import PatchCollection
        _, ax = plt.subplots()
        patches = [
            plt.Circle(center, radius)
            for center, radius in zip([pxy1, pxy2, pxy3], [r1, r2, r3])
        ]
        circles = PatchCollection(patches, color='c', lw=1.5, fc='none')
        c1 = plt.Circle(posa, ra, color='r', lw=1.5, fc='none')
        c2 = plt.Circle(posb, rb, color='b', lw=1.5, fc='none')
        ax.set_aspect(1)
        ax.add_collection(circles)
        ax.add_artist(c1)
        ax.add_artist(c2)
        x1, y1 = pxy1
        x2, y2 = pxy2
        x3, y3 = pxy3
        plt.plot(x1, y1, 'k*', x2, y2, 'k*', x3, y3, 'k*')
        plt.grid(linestyle='--')
        plt.show()

    return posa, ra, posb, rb


def cone(pos, p1, p2):
    """
    Check if the test point pos is inside the cone region between two nearest particles.
    """
    pos1 = p1[:3]
    pos2 = p2[:3]
    r1 = p1[3]
    r2 = p2[3]

    if r1 == r2:
        return height(pos, pos1, pos2) < r1
    l = norm(pos1 - pos2)
    theta = np.arcsin(np.abs(r1 - r2) / l)
    q = pos1 + (pos2 - pos1) * r1 / (r1 - r2)
    thetap = np.arccos(np.dot(q - pos, pos1 - pos) / norm(pos1 - pos) / l)
    return thetap <= theta if r1 > r2 else thetap >= (np.pi - theta)


def coplanar(ps):
    """
    Check if the points are in the same plane. This is done by check the rank of the matrix of particles ps. If the rank
    is 2 or less, the points are coplanar.
    """

    return matrix_rank(ps[1:, :3] - ps[0, :3]) <= 2


def touch(s0, ps):
    """
    Find the sphere snew that touches four particles.
    """

    def t4eq(x):
        return distance(x, ps)

    return fsolve(t4eq, s0)

    # ----- Crevice growth scenario ----- #


def toroidal(pos, ps):
    """
    Calculate the toroidal approximation of the crevice unduloid formed between two closest particles with the test
    point pos on its surface. The point pos should be in the cone region between ps.
    In the plane passing through the particle centers and pos, the poloidal center pos and radius of the poloidal circle
    rp1 (minor radius) is calculated using the Apollonius ccp solution, which is the first principal radius. The second
    principal radius rp2 is approximated as the minimal distance from the poloidal circle to the line connecting the two
    particle centers, which is the minor radius minus the major (toroidal) radius.
    Here we require positive r1 and negative rp2 for a convex bridge, and if rp2 is positive, corresponding to a concave
    bridge, the solution is abandoned and we report zeros for rp1 and rp2.
    """
    pos1 = ps[0, :3]
    pos2 = ps[1, :3]
    r1 = ps[0, 3]
    r2 = ps[1, 3]

    R = project(pos, pos1, pos2)
    pxy0 = pos @ R.transpose()
    pxy1 = pos1 @ R.transpose()
    pxy2 = pos2 @ R.transpose()
    # find the circle, and the minor radius
    pxy, rp1, *_ = apollonius(pxy1[:2], r1, pxy2[:2], r2, pxy0[:2], 0)
    pxy = np.append(pxy, pxy0[2])
    posx = pxy @ R

    # distance to the line connecting the two centers
    d = height(posx, pos1, pos2)  # major radius of the torus
    if (rp1 >= d) | (rp1 == 0):
        rp1 = 0
        rp2 = 0
    else:
        rp2 = rp1 - d  # this should be negative
    return rp1, rp2, posx


# ----- Pore growth scenario ----- #


def contain(snew, pos, particles):
    """
    Check if the new sphere snew contains the point pos, and does not cut any other particles.
    """

    ids, _ = near(snew, particles)
    pnear = particles[ids, :]

    return cut(pos, snew) & (not cut(snew, pnear))


def nudge(s, move):
    """
    Move the original sphere s with a step size move to a random direction.
    """
    theta = rand(1) * 2 * np.pi
    φ = np.arccos(1 - 2 * rand(1))
    vec = np.array(
        [np.cos(theta) * np.sin(φ),
         np.sin(theta) * np.sin(φ),
         np.cos(φ)])
    dp = move * np.append(vec.flatten(), 0)
    return s + dp


def bulge(s0, pnear):
    """
    Grow the original sphere s0 without cutting nearby spheres pnear.
    """

    snew = s0
    incMax = 1000
    inc = 1

    move = np.mean(pnear[:, 3]) / 20
    while inc < incMax:
        s = nudge(snew, move)
        s[3] = max(distance(s0, s), snew[3])
        if cut(s, pnear):
            # cut other particles
            inc += 1
        else:
            _, dr = near(s, pnear, 1)
            s[3] += dr  # bulge to touch the nearest sphere
            snew = s
    return snew


def tune(s, psorted, pos):
    """
    Slightly adjust the position of sphere s according to spheres around and test point pos.Slightly adjust the position
    of sphere s according to spheres around and test point pos.
    """

    # check if possible to touch four particles
    for i in range(3, len(psorted)):
        p4 = stack(psorted[i, :], psorted[:3, :])
        if not coplanar(p4):
            snew = touch(s, p4)
            if contain(snew, pos, psorted) & (snew[3] > s[3]):
                s = snew
    # check if possible to touch three particles
    p3 = stack(pos, psorted[:3, :])
    snew = touch(s, p3)
    if contain(snew, pos, psorted) & (snew[3] > s[3]):
        s = snew
    return s


def inscribe(pos, particles):
    """
    Find the largest sphere that contains the test point and bounded by surrounding particles using a random nudge-bulge
    method.
    """

    # twenty nearest particles, should be sufficient
    ids, _ = near(pos, particles)
    pnear = particles[ids, :]

    # check if it is a throat
    rnew, _, posnew = toroidal(pos, pnear)

    if not cone(pos, pnear[0, :], pnear[1, :]):
        rnew = rnew * 1000

    if not cut(np.append(posnew, rnew), pnear):
        s = np.append(posnew, rnew)
        ispore = False
    else:
        ispore = True
        # start nudge-bulge subroutine
        # initial position and radius
        s0 = np.append(pos, 0)
        s = s0

        # calculate three times and keep the largest solution
        for _ in range(3):
            sk = bulge(s0, pnear)

            # fine tune
            # without these the curves in sc and fcc calculations are not straight

            # update new nearlist
            ids, _ = near(sk, pnear)
            psorted = pnear[ids, :]
            sk = tune(sk, psorted, pos)
            # compare the result
            if sk[3] > s[3]:
                s = sk
    return s, ispore


# ----- Residual scenario ----- #


def residual(sp, s1, s2):
    """
    Find a coplanar sphere touching sp, s1, and s2.
    """

    pos0 = sp[:3]
    pos1 = s1[:3]
    pos2 = s2[:3]
    r0 = sp[3]
    r1 = s1[3]
    r2 = s2[3]

    R = project(pos0, pos1, pos2)
    pxy0 = pos0 @ R.transpose()
    pxy1 = pos1 @ R.transpose()
    pxy2 = pos2 @ R.transpose()
    # find the circle, and the minor radius
    pxya, ra, pxyb, rb = apollonius(pxy0[:2], r0, pxy1[:2], r1, pxy2[:2], r2)
    # choose the smaller one
    if rb > 0:
        pxyb = np.append(pxyb, pxy0[2])
        posx = pxyb @ R  # row vector
        s = np.append(posx, rb)
    else:
        pxya = np.append(pxya, pxy0[2])
        posx = pxya @ R
        s = np.append(posx, ra)
    return s


def crevice(s, pnear):
    """
    Find the largest crevice sphere bounded by sphere s and nearby spheres pnear.
    """

    paround = around(s, pnear)
    rc = 0
    if len(paround) >= 2:
        # find all crevices
        idpair = list(combinations(range(len(paround)), 2))

        for pair in idpair:
            sa1 = paround[pair[0], :]
            sa2 = paround[pair[1], :]
            sc = residual(s, sa1, sa2)
            if sc[3] > rc:
                rc = sc[3]
    return rc
