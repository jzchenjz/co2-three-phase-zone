#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Formation and stability of the CO2 hydrate layer in the marine sediment, where three-phase coexistence near the BHSZ. The figures can be reproduced by running
    $ R1, R2 = mc()
    $ plotmc(R1, R2)
    $ krmc(R1, R2)
    $ sensitivity(R1, R2)
"""
#%%
import itertools
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import loadmat
from tqdm import tqdm
from scipy.optimize import fsolve

import grow
import sediment

plt.rcParams['figure.dpi'] = 300
plt.style.use(['science', 'nature', 'no-latex'])

# basic setting
GP = 1e4  # sub-seabed hydrostatic pressure gradient 1e4 Pa/m
GT = 3e-2  # geothermal gradient 30 K/km
d0 = 100  # seawater depth
Tsf = 0.5  # seafloor temperature in celsius

# parameters
g = 9.81
R = 8.3144598
Vh = 135.4e-6
Pa = 101325  # atmospheric pressure
ρh = 1120
ρl = 1030  # seawater density (Sprivey et al., 2004)
ξ = ρh / ρl
f = 0.289
φ = 0.5
u = 1e-3 / 365 / 86400  # 1 mm/yr
k = 1e-15  # nominal permeability

colors = ['tab:blue', 'tab:red']

# ----- Constitutive behaviors ----- #


def profile(z, d=d0, GT=GT):
    """
    Calculate the pressure and temperature at a depth z below the seafloor with water depth d.
    """
    P0 = Pa + ρl * g * d

    return P0 + GP * z, Tsf + GT * z + 273.15


def boundary(d=d0, GT=GT):
    """
    Calculate the BHSZ depth for CO2 hydrate and vapor-liquid boundary given a water depth.
    """

    P0 = Pa + ρl * g * d
    T0 = Tsf + 273.15

    def fit(T):  # Wendland et al. (1999)
        return np.exp(-446.88395 + 57308.65 / T + 0.868805 * T) * 1e6

    def vapor(T):  # Nevers (2012)
        return 10**(9.7037 - 863.35 / T)

    z3 = fsolve(lambda z: fit(T0 + GT * z) - P0 - GP * z, 300)[0]
    zv = fsolve(lambda z: vapor(T0 + GT * z) - P0 - GP * z, 300)[0]

    return z3, zv, P0 + GP * z3, T0 + GT * z3


def pr(P, T):
    """
    Calculate compressibility factor using the Peng-Robinson equation of state.
    """
    R = 8.314
    Pc = 7.37E+06
    Tc = 304.12
    ω = 0.225

    Tr = T / Tc

    α = (1 + (0.37464 + 1.54226 * ω - 0.26992 * ω**2) * (1 - np.sqrt(Tr)))**2
    a = 0.45724 * R**2 * Tc**2 / Pc
    b = 0.07780 * R * Tc / Pc
    A = α * a * P / T**2 / R**2
    B = b * P / R / T

    def fpr(z):
        return z**3 + (B - 1) * z**2 + z * (A - 2 * B - 3 * B**2) - (
            A * B - B**2 - B**3)

    return fsolve(fpr, 1)[0]


def gradient(d=d0, GT=GT):
    """
    Calculate the depth gradient of ln(x) near the three-phase equilibrium.
    """

    R = 8.314

    ΔHgl = -2.024e4
    ΔHhl = 4.23e4

    _, _, P3, T3 = boundary(d, GT)

    λ = 6

    Vh = 135.4e-6  # cm^3/mol
    pVm = 28.4e-6
    Vw = 17.93e-6
    Δv = Vh - pVm - λ * Vw
    Z = pr(P3, T3)
    # should depend on depth, but it's a good approximate

    # solubility gradient near P3 and T3
    g_gl = ΔHgl / R / T3**2 * GT + Z * GP / P3
    g_hl = ΔHhl / R / T3**2 * GT + Δv / R / T3 * GP
    return g_gl, g_hl, P3, T3


# ----- Three phase coexistence ----- #


def offset(rh, rg, P3, T3, Δg, GT=GT):
    """
    Calculate the offset of the three-phase zone with hydrate and gas radii.
    """

    if min(rh, rg) < 1e-10:
        return np.nan

    R = 8.314
    γgl = 0.07
    γhl = 0.029

    Vh = 135.4e-6

    A = 2 * Vh * γhl / R / rh
    B = 2 * γgl / rg

    def fdz(z):
        return A / (T3 + GT * z) - np.log(1 + B / (P3 + GP * z)) - Δg * z

    return fsolve(fdz, 0)[0]


# --- Monte Carlo ----- #


def mc(packfile=None, k=15, showplot=False):
    """
    First find the largest sphere containing the test point, and then find the residual sphere within the crevice touch
    the first sphere and two grains.
    """

    if packfile is None:
        packfile = 'data/Finney.mat'

    particles = loadmat(packfile)['particles']
    meanR = np.median(particles[:, 3])

    testR = k * meanR
    z = np.mean(particles[:, 2])
    particles = sediment.select(particles, z)

    P = 5000
    print('Number of test points: ', P)

    ppos = sediment.sample(particles, z, testR, P, showplot)

    if showplot:
        # view the test points
        sediment.xsection(particles, z, k)
        plt.plot(ppos[:, 0], ppos[:, 1], 'rx', markersize=2)

    # Monte Carlo
    R1 = np.zeros(P)
    R2 = np.zeros(P)

    for i in tqdm(range(P)):

        ptest = ppos[i, :]

        ids, _ = grow.near(ptest, particles)
        pnear = particles[ids, :]

        if not grow.cut(ptest, pnear):
            # not within a particle, calculate phase II crevice, suggesting impingement
            # it is OK that the crevice sphere cuts other particles
            if grow.cone(ptest, pnear[0, :], pnear[1, :]):
                r, _, pos = grow.toroidal(ptest, pnear[:2, :])
                s2 = np.append(pos, r)
                R2[i] = r
            else:
                # pore and throat case
                s2, _ = grow.inscribe(ptest, pnear)
                R2[i] = s2[3]

            # calculate phase I
            if R2[i] > 0:
                R1[i] = grow.crevice(s2, pnear)
    return R1, R2


def coexistmc(R1, R2, Rm, P3, T3, Δg, GT=GT):
    """
    Calculate corresponding offsets of the Monte Carlo siμlation result and plot.
    """

    P = len(R1)
    S2 = 1 - np.arange(1, P + 1) / P

    ind = np.argsort(R2)
    R2 = R2[ind] * Rm
    R1 = R1[ind] * Rm

    # bottom, hydrate is phase II
    dzb = np.array(
        [offset(R2[i], R1[i], P3, T3, Δg, GT) for i in range(np.size(R1))],
        dtype=float)
    # top, gas is phase II
    dzt = np.array(
        [offset(R1[i], R2[i], P3, T3, Δg, GT) for i in range(np.size(R2))],
        dtype=float)
    return dzt, dzb, S2


def plotmc(R1, R2):
    """
    Make Figure 2.
    """

    g_gl, g_hl, P3, T3 = gradient()
    Δg = g_gl - g_hl
    Rm = [1e-4, 1e-5, 1e-6]

    ind = np.where(R1 > 0)[0]
    R1 = R1[ind]
    R2 = R2[ind]

    plt.figure(figsize=(9, 4))
    for i, R in enumerate(Rm):
        dzt, dzb, S2 = coexistmc(R1, R2, R, P3, T3, Δg, GT)
        plt.subplot(1, len(Rm), i + 1)
        plt.plot(S2, dzt, '.', color=colors[0], markersize=2)
        plt.plot(S2, dzb, '.', color=colors[1], markersize=2)
        plt.title(rf'$R = 10^{{{-4-i}}}$ m')
        plt.xlim([0, 1])
        plt.xticks([0, 0.2, 0.4, 0.6, 0.8, 1])
        plt.xlabel(r'$S_h$ (red) and $S_g$ (blue)', fontsize=13)
        plt.legend(['top', 'bottom'],
                   frameon=False,
                   loc='lower left',
                   markerscale=5)
        ax = plt.gca()
        if i == 0:
            plt.ylim([0, 20])
            plt.ylabel(r'$\Delta z$'
                       "\n(m)",
                       rotation='horizontal',
                       fontsize=13)
            ax.yaxis.set_label_coords(-0.2, 0.5)
        elif i == 1:
            plt.ylim([0, 100])
        else:
            plt.ylim([0, 350])
        plt.gca().invert_yaxis()

    # plt.show()
    plt.savefig('co2_mc.png', bbox_inches='tight')


#%%
def sensitivity(R1, R2):
    """
    Make Figure 4.
    Test the sensitivity of the three-phase zone to the water depth and geothermal gradient.
    """
    d = np.array([90, 100, 110])
    GT = np.array([2.5, 3, 3.5]) * 1e-2

    ind = np.where(R2 > 0)[0]
    R1 = R1[ind]
    R2 = R2[ind]

    Rm = 1e-5
    plt.figure(figsize=(8, 8))
    alphabet = [chr(value) for value in range(97, 123)]
    for k, (i, j) in enumerate(itertools.product(range(3), range(3))):
        g_gl, g_hl, P3, T3 = gradient(d[i], GT[j])
        Δg = g_gl - g_hl
        dzt, dzb, S2 = coexistmc(R1, R2, Rm, P3, T3, Δg, GT[j])
        plt.subplot(3, 3, i * 3 + j + 1)
        plt.plot(S2, dzt, '.', color=colors[0], markersize=0.6)
        plt.plot(S2, dzb, '.', color=colors[1], markersize=0.6)
        plt.legend(['top', 'bottom'],
                   fontsize=12,
                   frameon=False,
                   loc='best',
                   markerscale=5)

        # add text
        ax = plt.gca()
        plt.text(0.33,
                 0.33,
                 rf'$d$ = {d[i]} m',
                 fontsize=12,
                 transform=ax.transAxes)
        plt.text(0.1,
                 0.5,
                 f'({alphabet[k]})',
                 fontsize=16,
                 transform=ax.transAxes)
        plt.ylim([0, 200])
        plt.xlim([0, 1])
        if i == 0:
            plt.title(f'$G_T={int(GT[j] * 1000)}$ K/km')
        if j == 0:
            plt.ylabel(r'$\Delta z$'
                       "\n(m)",
                       rotation='horizontal',
                       fontsize=13)
            ax.yaxis.set_label_coords(-0.2, 0.5)
        if i == 2:
            plt.xlabel(r'$S_h$ (red) and $S_g$ (blue)', fontsize=13)
        if i < 2:
            plt.gca().set_xticklabels([])
        if j > 0:
            plt.gca().set_yticklabels([])

        ax.invert_yaxis()
    # plt.show()
    plt.savefig('co2_dGT.png', bbox_inches='tight')


# --- Permeability reduction ----- #


def genuchten(Sl):
    """
    Calculate the permeability reduction given the liquid saturation Sl using the van Genuchten formula.
    """

    n = 2
    m = 1 - 1 / n
    return np.sqrt(Sl) * (1 - (1 - Sl**(1 / m))**m)**2


def krmc(R1, R2):
    """
    Calculate the permeability reduction due to phase I and II.
    """

    ind = np.where(R1 > 0)[0]
    R1 = R1[ind]
    R2 = R2[ind]
    P = len(R2)

    S2 = 1 - np.arange(1, P + 1) / P

    ind = np.argsort(R2, axis=0)
    R2 = R2[ind]
    R1 = R1[ind]

    R = 1
    δ = np.arcsin(R / (R + R2))
    # bottom, hydrate is phase II
    V1 = 4 * np.pi * R1**3 / 3  # in Chen et al. (2021) the 4/3 factor is missing
    Sl = (1 - S2) * (1 - V1 / 6 / np.pi / R2**2 /
                     (R - δ * np.sqrt(R2 * (R2 + 2 * R))))
    Sl[Sl < 0] = np.nan
    S1 = 1 - Sl - S2
    krt = genuchten(Sl)
    krh = genuchten(1 - S2)

    def plotkr(ymin, ylabel):
        plt.xlim([0, 1])
        plt.ylim([ymin, 1])
        plt.xlabel(r'$S_h$', fontsize=13)
        plt.ylabel(ylabel, fontsize=13, rotation='horizontal')

    plt.figure(figsize=(11, 3))
    plt.subplot(1, 3, 1)
    ax = plt.gca()
    plt.plot(S2, S1, '.')
    ax.axline((0, 1), slope=-1, color='k', lw=0.5, ls='--')
    plt.grid(linestyle=':')
    plotkr(0, r'$S_g$')
    plt.text(0.8, 0.4, '(a)', fontsize=16, transform=ax.transAxes)
    ax.yaxis.set_label_coords(-0.1, 0.5)
    plt.subplot(1, 3, 2)
    ax = plt.gca()
    plt.plot(S2, krt, 'r.', label=r'$k_{rt}$')
    plt.plot(S2, krh, 'k', lw=1.5, label=r'$k_{rh}$')
    plt.yscale('log')
    plotkr(1e-5, r'$k_r$')
    plt.legend(loc='best', frameon=False, fontsize=10)
    plt.grid(linestyle=':')
    plt.text(0.8, 0.4, '(b)', fontsize=16, transform=ax.transAxes)
    ax.yaxis.set_label_coords(-0.1, 0.5)
    plt.subplot(1, 3, 3)
    ax = plt.gca()
    plt.plot(S2, krt / krh, 'm.')
    plotkr(0, r'$\frac{k_{rt}}{k_{rh}}$')
    plt.text(0.8, 0.4, '(c)', fontsize=16, transform=ax.transAxes)
    ax.yaxis.set_label_coords(-0.12, 0.5)
    # plt.show()
    plt.savefig('kr_mc.png', bbox_inches='tight')
