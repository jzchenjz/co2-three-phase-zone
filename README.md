# CO2 three-phase zone

This repository contains python scripts to simulate the formation of CO~2~ three-phase zone in sub-seabed sediments for the publication [*Gas-saturated carbon dioxide hydrates above sub-seabed carbon sequestration site and the formation of self-sealing cap*](https://www.sciencedirect.com/science/article/abs/pii/S2949908923000419). The main functions are in `co2cap.py`, with other auxiliary functions in `grow.py` and `sediment.py`.

The data required are the Finney pack (`Finney.mat`) in the `data` directory.
